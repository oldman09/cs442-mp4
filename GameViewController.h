//
//  GameViewController.h
//  MP4
//
//  Created by Yongzhen Qiu on 14-5-7.
//  Copyright (c) 2014年 Yongzhen Qiu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "BoardView.h"
#import "gameModel.h"
#import <Parse/Parse.h>
@interface GameViewController : UIViewController<BoardViewDelegate,UIAlertViewDelegate>
@property (strong, nonatomic) BoardView *boardView;
@property(strong , nonatomic) gameModel *game;
@property(strong, nonatomic) PFObject *gameOnline;
@property int PlayerNO;
@property BOOL moved;

@end
