//
//  GameViewController.m
//  MP4
//
//  Created by Yongzhen Qiu on 14-5-7.
//  Copyright (c) 2014年 Yongzhen Qiu. All rights reserved.
//

#import "GameViewController.h"

@interface GameViewController ()

@end

@implementation GameViewController{
    int numberOfTaps[7];
    //int PlayerNO;
    BOOL findWinner;

}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Do any additional setup after loading the view.
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    findWinner=NO;
    //self.PlayerNO=1;
    self.boardView = [[BoardView alloc] initWithFrame:self.view.bounds slotDiameter:36];
    self.boardView.delegate = self;
    [self.view addSubview:self.boardView];
    self.game=[[gameModel alloc] initWithArray:[self.gameOnline objectForKey:@"model"]];
    for (int i=0; i<6; i++) {
        for (int j=0; j<7; j++) {
            if ([[[[self.gameOnline objectForKey:@"model"] objectAtIndex:i] objectAtIndex:j] intValue]==1) {
                UIView *piece = [[UIView alloc]
                                 initWithFrame:CGRectMake((j+1)*self.boardView.gridWidth-self.boardView.slotDiameter/2.0,
                                                          (6-i)*self.boardView.gridHeight+44,
                                                          self.boardView.slotDiameter,
                                                          self.boardView.slotDiameter)];
                piece.backgroundColor = [UIColor redColor];
                [self.view insertSubview:piece belowSubview:self.boardView];
                piece.center=CGPointMake((j+1)*self.boardView.gridWidth, (6-numberOfTaps[j])*self.boardView.gridHeight+44);
                numberOfTaps[j]++;
                
            }
            if ([[[[self.gameOnline objectForKey:@"model"] objectAtIndex:i] objectAtIndex:j] intValue]==2) {
                UIView *piece = [[UIView alloc]
                                 initWithFrame:CGRectMake((j+1)*self.boardView.gridWidth-self.boardView.slotDiameter/2.0,
                                                          (6-i)*self.boardView.gridHeight+44,
                                                          self.boardView.slotDiameter,
                                                          self.boardView.slotDiameter)];
                piece.backgroundColor = [UIColor yellowColor];
                [self.view insertSubview:piece belowSubview:self.boardView];
                piece.center=CGPointMake((j+1)*self.boardView.gridWidth, (6-numberOfTaps[j])*self.boardView.gridHeight+44);
                numberOfTaps[j]++;
                
            }
            
            
            
        }
    }
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)boardView:(BoardView *)boardView columnSelected:(int)column
{
    if (self.moved==NO&&findWinner==NO&&numberOfTaps[column-1]<6) {
        
        self.moved=YES;
        self.gameOnline[@"newGame"]=[NSNumber numberWithBool:NO];
        self.gameOnline[@"lastMoveBy"]=[PFUser currentUser];
        if (self.PlayerNO==2) {
            self.gameOnline[@"palyer"]=[PFUser currentUser];
        }
        UIView *piece = [[UIView alloc]
                         initWithFrame:CGRectMake(column*self.boardView.gridWidth-self.boardView.slotDiameter/2.0,
                                                  -self.boardView.slotDiameter,
                                                  self.boardView.slotDiameter,
                                                  self.boardView.slotDiameter)];
        if (self.PlayerNO==1) {
            [self.game doMovementAt:numberOfTaps[column-1] and:column-1 forPlayer:self.PlayerNO];
            NSMutableArray *array=[[NSMutableArray alloc] initWithArray:[[self.gameOnline objectForKey:@"model"] objectAtIndex:numberOfTaps[column-1]]];
            [array replaceObjectAtIndex:column-1 withObject:@1];
            piece.backgroundColor = [UIColor redColor];
            self.PlayerNO=2;
            [[self.gameOnline objectForKey:@"model"] setObject:array atIndex:numberOfTaps[column-1]];

            
        }else{
            [self.game doMovementAt:numberOfTaps[column-1] and:column-1 forPlayer:self.PlayerNO];
            NSMutableArray *array=[[NSMutableArray alloc] initWithArray:[[self.gameOnline objectForKey:@"model"] objectAtIndex:numberOfTaps[column-1]]];
            [array replaceObjectAtIndex:column-1 withObject:@2];
            piece.backgroundColor = [UIColor yellowColor];
            self.PlayerNO=1;
            [[self.gameOnline objectForKey:@"model"] setObject:array atIndex:numberOfTaps[column-1]];

        }
        [self.gameOnline saveInBackground];
        
        double durationTime=sqrt((6-numberOfTaps[column-1])*self.boardView.gridHeight*2/800);
        CABasicAnimation * animation = [CABasicAnimation animationWithKeyPath:@"position"];
        [animation setFromValue:[NSValue valueWithCGPoint:piece.center]];
        [animation setToValue:[NSValue valueWithCGPoint:CGPointMake(column*self.boardView.gridWidth, (6-numberOfTaps[column-1])*self.boardView.gridHeight+44)]];
        [animation setDuration:durationTime];
        [animation setTimingFunction:[CAMediaTimingFunction functionWithControlPoints:.4 :0 :0.7 :0]];
        
        [self.view insertSubview:piece belowSubview:self.boardView];
        [piece.layer addAnimation:animation forKey:@"gravity"];
        piece.center = CGPointMake(column*self.boardView.gridWidth, (6-numberOfTaps[column-1])*self.boardView.gridHeight+44);
        
        //        [self.view insertSubview:piece belowSubview:self.boardView];
        //        [UIView animateWithDuration:1 animations:^{
        //            piece.center = CGPointMake(100+column*self.boardView.gridWidth, (6-numberOfTaps[column-1])*self.boardView.gridHeight);
        //    }];
        numberOfTaps[column-1]++;
        
        
        int winner=[self.game checkWinner];
        if (winner==1) {
            UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Game Over" message:@"Player 1 Win!" delegate:self cancelButtonTitle:@"Restart Game" otherButtonTitles:@"Cancel", nil];
            [alert show];
            findWinner=YES;
            self.gameOnline[@"finished"]=[NSNumber numberWithBool:YES];
        }
        if (winner==2) {
            UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Game Over" message:@"Player 2 Win!" delegate:self cancelButtonTitle:@"Restart Game" otherButtonTitles:@"Cancel", nil];
            [alert show];
            findWinner=YES;
            self.gameOnline[@"finished"]=[NSNumber numberWithBool:YES];
            
        }
        self.gameOnline[@"step"]=[NSNumber numberWithInt:[self.gameOnline[@"step"] intValue]+1];
        [self.gameOnline saveInBackground];

        
        
    }
    
    
}




/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
