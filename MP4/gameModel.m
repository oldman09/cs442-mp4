//
//  gameModel.m
//  MP3
//
//  Created by Yongzhen Qiu on 14-4-23.
//  Copyright (c) 2014年 Yongzhen Qiu. All rights reserved.
//

#import "gameModel.h"


@implementation gameModel{
    int gameMatrix[6][7];
}
-(id)initWithArray:(NSArray *)array{
    self=[super init];
    if (self) {
        for (int i=0; i<6; i++) {
            for (int j=0; j<7; j++) {
                gameMatrix[i][j]=[[[array objectAtIndex:i] objectAtIndex:j] intValue];
            }
        }
    }

    return self;

}

-(id)init{
    self=[super init];
    if (self) {
        for (int i=0; i<6; i++) {
            for (int j=0; j<7; j++) {
                gameMatrix[i][j]=0;
            }
        }
    }
    return self;
}


-(void)doMovementAt:(int)row and:(int)column forPlayer:(int)playerNumber{
    gameMatrix[row][column]=playerNumber;

}

-(int)checkWinner{
    int winner=0;
    for (int i=0; i<3; i++) {
        for (int j=0; j<7; j++) {
            if (gameMatrix[i][j]==gameMatrix[i+1][j]&&gameMatrix[i+1][j]==gameMatrix[i+2][j]&&gameMatrix[i+2][j]==gameMatrix[i+3][j]) {
                if (gameMatrix[i][j]!=0) {
                    winner=gameMatrix[i][j];
                }
                
            }
        }
    }
    for (int i=0; i<6; i++) {
        for (int j=0; j<4; j++) {
            if (gameMatrix[i][j]==gameMatrix[i][j+1]&&gameMatrix[i][j+1]==gameMatrix[i][j+2]&&gameMatrix[i][j+2]==gameMatrix[i][j+3]) {
                if (gameMatrix[i][j]!=0) {
                    winner=gameMatrix[i][j];
                }
            }
        }
    }
    for (int i=0; i<3; i++) {
        for (int j=0; j<4; j++) {
            if (gameMatrix[i][j]==gameMatrix[i+1][j+1]&&gameMatrix[i+1][j+1]==gameMatrix[i+2][j+2]&&gameMatrix[i+2][j+2]==gameMatrix[i+3][j+3]) {
                if (gameMatrix[i][j]!=0) {
                    winner=gameMatrix[i][j];
                }
            }
        }
    }
    for (int i=0; i<3; i++) {
        for (int j=3; j<7; j++) {
            if (gameMatrix[i][j]==gameMatrix[i+1][j-1]&&gameMatrix[i+1][j-1]==gameMatrix[i+2][j-2]&&gameMatrix[i+2][j-2]==gameMatrix[i+3][j-3]) {
                if (gameMatrix[i][j]!=0) {
                    winner=gameMatrix[i][j];
                }
            }
        }
    }
    return winner;
    
}

@end
