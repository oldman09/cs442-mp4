//
//  main.m
//  MP4
//
//  Created by Yongzhen Qiu on 14-5-2.
//  Copyright (c) 2014年 Yongzhen Qiu. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MP4AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([MP4AppDelegate class]));
    }
}
