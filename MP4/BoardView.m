//
//  BoardView.m
//  MP3
//
//  Created by Yongzhen Qiu on 14-4-22.
//  Copyright (c) 2014年 Yongzhen Qiu. All rights reserved.
//

#import "BoardView.h"

@implementation BoardView{
    NSMutableArray *_columnViews;
    double boardWidth;
}

- (id)initWithFrame:(CGRect)frame slotDiameter:(double)diameter
{
    self = [super initWithFrame:frame];
    if (self) {
        boardWidth=self.bounds.size.width;
        self.opaque = NO;
        _gridWidth  = boardWidth / 8;
        _gridHeight = (self.bounds.size.height-44) / 7;
        _slotDiameter = diameter;
        _columnViews = [NSMutableArray array];
        [self addSubviews];
    }
    return self;
}

-(void)addSubviews{
    for (int i=1; i<=7; i++) {
        UIView *columnView = [[UIView alloc] initWithFrame:CGRectMake(_gridWidth*i-_slotDiameter/2.0, 0, _slotDiameter, self.bounds.size.height)];
        columnView.backgroundColor = [UIColor clearColor];
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(columnTapped:)];
        tap.numberOfTapsRequired = 1;
        tap.numberOfTouchesRequired = 1;
        
        [columnView addGestureRecognizer:tap];
        
        [_columnViews addObject:columnView];
        [self addSubview:columnView];
    }

}
- (void)columnTapped:(UIGestureRecognizer *)gestureRecognizer
{
    int idx = (int)[_columnViews indexOfObject:gestureRecognizer.view];
    [self.delegate boardView:self columnSelected:idx+1];
}
- (void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [UIColor orangeColor].CGColor);
    CGRect boardRect=CGRectMake(0, 44, boardWidth, self.bounds.size.height);
    CGContextAddRect(context, boardRect);
    for (int i=1; i<=7; i++) {
        for (int j=1; j<=6; j++) {
            CGContextAddEllipseInRect(context, CGRectMake(i*_gridWidth-_slotDiameter/2.0,
                                                          44+j*_gridHeight-_slotDiameter/2.0,
                                                          _slotDiameter,
                                                          _slotDiameter));
        }
    }
    CGContextEOFillPath(context);
}


@end
