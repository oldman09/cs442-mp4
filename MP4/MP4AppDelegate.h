//
//  MP4AppDelegate.h
//  MP4
//
//  Created by Yongzhen Qiu on 14-5-2.
//  Copyright (c) 2014年 Yongzhen Qiu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MP4AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
