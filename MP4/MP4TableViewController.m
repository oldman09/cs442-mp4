//
//  MP4TableViewController.m
//  MP4
//
//  Created by Yongzhen Qiu on 14-5-7.
//  Copyright (c) 2014年 Yongzhen Qiu. All rights reserved.
//

#import "MP4TableViewController.h"
#import <Parse/Parse.h>
#import <QuartzCore/QuartzCore.h>
#import "GameViewController.h"


@interface MP4TableViewController ()

@end

@implementation MP4TableViewController{

    NSMutableArray *newGame;
    NSMutableArray *availableGame;
    NSMutableArray *finishedGame;
    NSMutableArray *waitingGame;
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    //[self queryToRefresh];

    //self.tableView.tableFooterView = nil;
    

}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    NSLog(@"%@",[PFUser currentUser]);
    if (![PFUser currentUser]) {
        PFLogInViewController *logInViewController = [[PFLogInViewController alloc] init];
        [logInViewController setDelegate:self];
        PFSignUpViewController *signUpViewController = [[PFSignUpViewController alloc] init];
        [signUpViewController setDelegate:self];
        [logInViewController setSignUpController:signUpViewController];
        [self presentViewController:logInViewController animated:YES completion:NULL];
    }
    else
    [self queryToRefresh];
}

- (void)logInViewController:(PFLogInViewController *)logInController didLogInUser:(PFUser *)user {
    [self dismissViewControllerAnimated:YES completion:NULL];
    //[self queryToRefresh];
}

- (void)signUpViewController:(PFSignUpViewController *)signUpController didSignUpUser:(PFUser *)user{
    [self dismissViewControllerAnimated:YES completion:NULL];
    //[self queryToRefresh];
}

-(void)queryToRefresh{
    PFQuery *availableGameQuery=[[PFQuery alloc] initWithClassName:@"Game"];
    PFQuery *waitingGameQuery=[[PFQuery alloc] initWithClassName:@"Game"];
    PFQuery *finishedGameQuery=[[PFQuery alloc] initWithClassName:@"Game"];
    PFQuery *newGameQuery1=[[PFQuery alloc] initWithClassName:@"Game"];
    
    PFQuery *newGameQuery2=[[PFQuery alloc] initWithClassName:@"Game"];
    
    [newGameQuery1 whereKey:@"starter" equalTo:[PFUser currentUser]];
    [newGameQuery1 whereKey:@"lastMoveBy" notEqualTo:[PFUser currentUser]];

    [newGameQuery2 whereKey:@"palyer" equalTo:[PFUser currentUser]];
    [newGameQuery2 whereKey:@"lastMoveBy" notEqualTo:[PFUser currentUser]];
    PFQuery *newGameQuery=[PFQuery orQueryWithSubqueries:@[newGameQuery1,newGameQuery2]];
    [newGameQuery includeKey:@"starter"];
    [newGameQuery includeKey:@"palyer"];
    
    [newGameQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            newGame=[[NSMutableArray alloc] initWithArray:objects];
            [self.tableView reloadData];
        }
    }];
    
    [availableGameQuery whereKey:@"starter" notEqualTo:[PFUser currentUser]];
    [availableGameQuery whereKey:@"lastMoveBy" notEqualTo:[PFUser currentUser]];
    [availableGameQuery whereKey:@"finished" equalTo:[NSNumber numberWithBool:NO]];
    [availableGameQuery includeKey:@"starter"];
    [availableGameQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            availableGame=[[NSMutableArray alloc] initWithArray:objects];
            [self.tableView reloadData];
        }
    }];
    [waitingGameQuery whereKey:@"lastMoveBy" equalTo:[PFUser currentUser]];
    [waitingGameQuery whereKey:@"finished" equalTo:[NSNumber numberWithBool:NO]];
    [waitingGameQuery includeKey:@"starter"];
    [waitingGameQuery includeKey:@"palyer"];
    [waitingGameQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        waitingGame=[[NSMutableArray alloc] initWithArray:objects];
        [self.tableView reloadData];
    }];
    [finishedGameQuery whereKey:@"finished" equalTo:[NSNumber numberWithBool:YES]];
    [finishedGameQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        finishedGame=[[NSMutableArray alloc] initWithArray:objects];
        [self.tableView reloadData];
    }];
    [NSThread sleepForTimeInterval:0.5];
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger numberOfRows;
    switch (section)
    {
        case 0:
            numberOfRows=newGame.count;
            break;
        case 1:
            numberOfRows=waitingGame.count;
            break;
        case 2:
            numberOfRows=availableGame.count;
            break;
        case 3:
            numberOfRows=finishedGame.count;
            break;
            
        default:
            numberOfRows=0;
            break;
    }
    return numberOfRows;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    if (!cell) {
        cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    UILabel *userFlag1=(UILabel *) [cell.contentView viewWithTag:1];
    UILabel *username1=(UILabel *) [cell.contentView viewWithTag:2];
    UILabel *userFlag2=(UILabel *) [cell.contentView viewWithTag:3];
    UILabel *username2=(UILabel *) [cell.contentView viewWithTag:4];

    switch (indexPath.section)
    {
        case 0:
            userFlag1.backgroundColor=[UIColor redColor];
            [userFlag1.layer setCornerRadius:19.0];
            userFlag2.backgroundColor=[UIColor yellowColor];
            [userFlag2.layer setCornerRadius:19.0];
            username1.text=[[[newGame objectAtIndex:indexPath.row] objectForKey:@"starter"] objectForKey:@"username"];
            if ([[[newGame objectAtIndex:indexPath.row] objectForKey:@"palyer"] objectForKey:@"username"]) {
                username2.text=[[[newGame objectAtIndex:indexPath.row] objectForKey:@"palyer"] objectForKey:@"username"];
            }else{
                username2.text=@"No opponent yet";
            }            break;
        case 1:
            userFlag1.backgroundColor=[UIColor redColor];
            [userFlag1.layer setCornerRadius:19.0];
            userFlag2.backgroundColor=[UIColor yellowColor];
            [userFlag2.layer setCornerRadius:19.0];
            username1.text=[[[waitingGame objectAtIndex:indexPath.row] objectForKey:@"starter"] objectForKey:@"username"];
            if ([[[waitingGame objectAtIndex:indexPath.row] objectForKey:@"palyer"] objectForKey:@"username"]) {
                username2.text=[[[waitingGame objectAtIndex:indexPath.row] objectForKey:@"palyer"] objectForKey:@"username"];
            }else{
                username2.text=@"No opponent yet";
            }
            break;
        case 2:
            cell.textLabel.text=[NSString stringWithFormat:@"Game created by %@", [[[availableGame objectAtIndex:indexPath.row] objectForKey:@"starter"] objectForKey:@"username"]];
            break;
        case 3:
            break;
        default:
            break;
    }

    
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    NSString *sectionName;
    
    switch (section)
    {
        case 0:
            sectionName =@"Your Turn";
            break;
        case 1:
            sectionName =@"Their Turn";
            break;
        case 2:
            sectionName =@"Available Games";
            break;


        case 3:
            sectionName =@"Finished Games";
            break;

            // ...
        default:
            sectionName = @"";
            break;
    }
    return sectionName;

}


- (IBAction)creatNewGame:(UIBarButtonItem *)sender {
    PFObject *game=[PFObject objectWithClassName:@"Game"];
    PFUser *user=[PFUser currentUser];
    [game setObject:user forKey:@"starter"];
    game[@"finished"]=[NSNumber numberWithBool:NO];
    game[@"availability"]=[NSNumber numberWithBool:NO];
    game[@"newGame"]=[NSNumber numberWithBool:YES];
    game[@"waitingGame"]=[NSNumber numberWithBool:NO];
    game[@"step"]=[NSNumber numberWithInt:0];
    NSMutableArray *gameMatrix=[[NSMutableArray alloc] init];
    for (int i=0; i<6; i++) {
        NSArray *gameRow=@[@0,@0,@0,@0,@0,@0,@0];
        [gameMatrix addObject:gameRow];
        
    }
    game[@"model"]=gameMatrix;
    [game saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (succeeded) {
            [self queryToRefresh];
        }
    }];

}

- (IBAction)logoutButtonPressed:(id)sender {
    [PFUser logOut];
}

- (BOOL)logInViewController:(PFLogInViewController *)logInController shouldBeginLogInWithUsername:(NSString *)username password:(NSString *)password {
    // Check if both fields are completed
    if (username && password && username.length != 0 && password.length != 0) {
        return YES; // Begin login process
    }
    
    [[[UIAlertView alloc] initWithTitle:@"Missing Information"
                                message:@"Make sure you fill out all of the information!"
                               delegate:nil
                      cancelButtonTitle:@"ok"
                      otherButtonTitles:nil] show];
    return NO; // Interrupt login process
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"toGame"]) {
        NSIndexPath *indexPath=[self.tableView indexPathForSelectedRow];
        
                
        
        
        switch (indexPath.section)
        {
            case 0:
                [(GameViewController *)[segue destinationViewController] setGameOnline:[newGame objectAtIndex:indexPath.row]];
                [(GameViewController *)[segue destinationViewController] setPlayerNO:([[[newGame objectAtIndex:indexPath.row] objectForKey:@"step"] intValue]+1)%2];
                [(GameViewController *)[segue destinationViewController] setMoved:NO];
                break;
            case 1:
                [(GameViewController *)[segue destinationViewController] setGameOnline:[waitingGame objectAtIndex:indexPath.row]];
                [(GameViewController *)[segue destinationViewController] setMoved:YES];

                break;
            case 2:
                [(GameViewController *)[segue destinationViewController] setGameOnline:[availableGame objectAtIndex:indexPath.row]];
                [(GameViewController *)[segue destinationViewController] setPlayerNO:2];
                [(GameViewController *)[segue destinationViewController] setMoved:NO];
                break;
            case 3:
                [(GameViewController *)[segue destinationViewController] setGameOnline:[finishedGame objectAtIndex:indexPath.row]];
                [(GameViewController *)[segue destinationViewController] setPlayerNO:2];
                [(GameViewController *)[segue destinationViewController] setMoved:NO];
                break;
            default:
                break;
        }
    }
}





@end
