//
//  gameModel.h
//  MP3
//
//  Created by Yongzhen Qiu on 14-4-23.
//  Copyright (c) 2014年 Yongzhen Qiu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface gameModel : NSObject

-(id)initWithArray:(NSArray *)array;

-(void)doMovementAt:(int)row and: (int)column forPlayer:(int) playerNumber;

-(int)checkWinner;
@end
