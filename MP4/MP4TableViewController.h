//
//  MP4TableViewController.h
//  MP4
//
//  Created by Yongzhen Qiu on 14-5-7.
//  Copyright (c) 2014年 Yongzhen Qiu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>

@interface MP4TableViewController : UITableViewController<UITableViewDataSource,UITableViewDelegate,PFLogInViewControllerDelegate,PFSignUpViewControllerDelegate>
- (IBAction)creatNewGame:(UIBarButtonItem *)sender;
- (IBAction)logoutButtonPressed:(id)sender;

-(void)queryToRefresh;
@end
